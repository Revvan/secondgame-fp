using System;
using UnityEngine;

public sealed class Sound_Settings : MonoBehaviour
{
    public static Sound_Settings instance;

    [SerializeField] private Sound_Class[] musicSounds, sfxSounds;
    public AudioSource musicSource, sfxSource;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PlayMusic("Theme");
    }


    public void PlayMusic(string name)
    {
        Sound_Class s = Array.Find(musicSounds, x => x.soundName == name);

        if (s == null)
        {
            Debug.LogWarning("Sound not found");
            return;
        }
        else
        {
            musicSource.clip = s.clip;
            musicSource.Play();
        }
    }
    public void PlaySFX(string name)
    {
        Sound_Class s = Array.Find(sfxSounds, x => x.soundName == name);

        if (s == null)
        {
            Debug.LogWarning("SFX sound not found");
            return;
        }
        else
        {
            sfxSource.PlayOneShot(s.clip);
        }
    }


    public void ToggleMusic()
    {
        musicSource.mute = !musicSource.mute;
    }
    public void ToggleSFX()
    {
        sfxSource.mute = !sfxSource.mute;
    }
    public void MusicVolume(float value)
    {
        musicSource.volume = value;
    }
    public void SFXVolume(float value)
    {
        sfxSource.volume = value;
    }
}