using UnityEngine;

[System.Serializable]
public class Sound_Class
{
    public string soundName;
    public AudioClip clip;
}