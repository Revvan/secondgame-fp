using UnityEngine;


public sealed class Game_Manager : MonoBehaviour
{
    public static Game_Manager instance;


    [Header("Win Condition references")]
    public int currentCoins;
    [SerializeField] private int goalCoins;


    [Header("Spawn coins references")]
    [SerializeField] private GameObject coinPrefab;
    [SerializeField] private float timeUntilNewCoinSpawn;
    private float resetSpawnCoinTime;

    [Header("Spawn traps references")]
    [SerializeField] private GameObject trapPrefab;
    [SerializeField] private float timeUntilNewTrapSpawn;
    private float resetSpawnTrapTime;


    private void Awake()
    {
        instance = this;

        resetSpawnCoinTime = timeUntilNewCoinSpawn;
        resetSpawnTrapTime = timeUntilNewTrapSpawn;
    }


    private void Update()
    {
        timeUntilNewCoinSpawn -= Time.deltaTime;
        timeUntilNewTrapSpawn -= Time.deltaTime;
        SpawnSystem();

        WinCondition();
        LostCondition();
    }


    private void WinCondition()
    {
        if (currentCoins == goalCoins)
        {
            UI_Controller.instance.Win();
            Sound_Settings.instance.musicSource.Stop();
            Debug.Log("Ganaste");
        }
    }
    private void LostCondition()
    {
        if (Pawn_Stats.Instance.currentHealth <= 0)
        {
            UI_Controller.instance.Lost();
            Sound_Settings.instance.musicSource.Stop();
            Debug.Log("Perdiste");
        }
    }


    private void SpawnSystem()
    {
        GameObject coin = coinPrefab;
        GameObject trap = trapPrefab;

        Vector3 randomPositionForCoins = new Vector3(Random.Range(345, 424), 1, Random.Range(450, 525));
        Vector3 randomPositionForTraps = new Vector3(Random.Range(345, 424), 0, Random.Range(450, 525));

        if (timeUntilNewCoinSpawn <= 0)
        {
            Instantiate(coin, randomPositionForCoins, Quaternion.identity);
            timeUntilNewCoinSpawn = resetSpawnCoinTime;
        }

        if (timeUntilNewTrapSpawn <= 0)
        {
            Instantiate(trap, randomPositionForTraps, Quaternion.identity);
            timeUntilNewTrapSpawn = resetSpawnTrapTime;
        }
    }
}