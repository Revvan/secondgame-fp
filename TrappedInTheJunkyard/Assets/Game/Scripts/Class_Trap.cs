using UnityEngine;

public sealed class Class_Trap : MonoBehaviour
{
    private Animator animator;


    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            animator.SetTrigger("Active");

            Sound_Settings.instance.PlaySFX("Crippled player SFX");
            Sound_Settings.instance.PlaySFX("Trap active");

            Pawn_Movement.Instance.slow = true;
            Class_Enemy.instance.trapIsActive = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("DecorationsBox") || other.gameObject.CompareTag("DocorationsMesh"))
        {
            Debug.Log("Trap has been destroyed");
            Destroy(this.gameObject);
        }
    }
    
    public void DestroyTrap()
    {
        Destroy(this.gameObject);
    }
}