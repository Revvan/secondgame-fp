using UnityEngine;

public class ZombieHand : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Sound_Settings.instance.PlaySFX("Player take damage");
            Pawn_Stats.Instance.TakeDamage(0.5f);
        }
    }
}