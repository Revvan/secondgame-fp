using UnityEngine;
using UnityEngine.AI;


public enum AlertStage
{
    Peaceful,
    Intrigued,
    Alerted
}


[RequireComponent(typeof(NavMeshAgent))]
public sealed class Class_Enemy : MonoBehaviour
{
    public static Class_Enemy instance;

    [Header("Agent reference")]
    private NavMeshAgent agent;


    [Header("Animation references")]
    private Animator animator;

    [SerializeField] private BoxCollider hand;


    [Header("Detection references")]
    private AlertStage alertStage;
    [Range(0, 100)] public float alertLevel;
    [SerializeField] private float increaseAggroRate;
    [SerializeField] private float decreaseAggroRate;

    [SerializeField] private float fov;

    [SerializeField] private Transform player;

    public bool trapIsActive;
    /* Conic field of view reference
    [Range(0 , 360)]
    [SerializeField] private float fovAngle;
    */


    [Header("Patrol system reference")]
    [SerializeField] private Transform[] waypoints;
    private int _currentWaypointIndex;
    [SerializeField] private float waypointCheck;


    private void Awake()
    {
        instance = this;

        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        alertStage = AlertStage.Peaceful;
        alertLevel = 0;

        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        AreaDetection();
        InvestigateArea();
    }


    private void AreaDetection()
    {
        bool playerInFoV = false;
        Collider[] targetsInFoV = Physics.OverlapSphere(transform.position, fov);

        foreach (Collider c in targetsInFoV)
        {
            if (c.CompareTag("Player"))
            {
                /* Conic field of view
                float signedAngle = Vector3.Angle(transform.forward, c.transform.position - transform.position);
                if(Mathf.Abs(signedAngle) < fovAngle/2)
                */

                playerInFoV = true;
                break;
            }
        }

        UpdateAlertState(playerInFoV);
    }
    private void UpdateAlertState(bool playerInFoV)
    {
        switch (alertStage)
        {
            case AlertStage.Peaceful:
                if (!playerInFoV)
                {
                    alertStage = AlertStage.Peaceful;
                    RoamArea();
                }
                else
                    alertStage = AlertStage.Intrigued;
                break;

            case AlertStage.Intrigued:
                if (playerInFoV)
                {
                    alertLevel += increaseAggroRate;

                    if (alertLevel >= 100)
                    {
                        Sound_Settings.instance.PlaySFX("Zombie Detect");
                        alertStage = AlertStage.Alerted;
                    }           
                }
                else
                {
                    alertLevel -= decreaseAggroRate;

                    if (alertLevel <= 0)
                        alertStage = AlertStage.Peaceful;
                }
                break;

            case AlertStage.Alerted:
                if (!playerInFoV)
                    alertStage = AlertStage.Intrigued;
                else
                {
                    alertStage = AlertStage.Alerted;
                    ChasePlayer();
                }
                break;
        }
    }


    private void RoamArea()
    {
        Transform wp = waypoints[_currentWaypointIndex];

        if (Vector3.Distance(transform.position, wp.position) < waypointCheck)
        {
            _currentWaypointIndex = (_currentWaypointIndex + 1) % waypoints.Length;
        }
        else
        {
            agent.SetDestination(wp.position);
            animator.SetBool("Run", true);
        }
    }

    private void InvestigateArea()
    {
        if (trapIsActive)
        {
            if (alertStage != AlertStage.Alerted)
            {
                alertLevel = 75f;
                alertStage = AlertStage.Intrigued;
                trapIsActive = false;
            }
        }
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
        
        if (Vector3.Distance(transform.position, player.position) < agent.stoppingDistance)
        {
            animator.SetBool("Run", false);
            animator.SetTrigger("Attack");
        }
        else
            animator.SetBool("Run", true);
    }
    public void EnableCollider()
    {
        hand.enabled = true;
    }
    public void DisableCollider()
    {
        hand.enabled = false;
    }
    public void PlaySFXAttackSound()
    {
        Sound_Settings.instance.PlaySFX("Zombie Attack");
    }
}