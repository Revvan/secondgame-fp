using System.Drawing;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Controller : MonoBehaviour
{
    public static UI_Controller instance;


    [Header("Player HUD")]
    [SerializeField] private Text currentHPText;
    [SerializeField] private Text currentCoinsText;
    [SerializeField] private Text missionBriefText;
    [SerializeField] private float hideBrief;

    [Header("Menu")]
    [SerializeField] private Text pauseText;
    [SerializeField] private Button restartButton;
    [SerializeField] private Text musicText;
    [SerializeField] private Text sfxText;
    [SerializeField] private Button quitButton;

    public Slider _musicSlider, _sfxSlider;
    private bool _isPause;

    [Header("Win Menu")]
    [SerializeField] private Text winText;
    [SerializeField] private Text creatorText;

    [Header("Lost Menu")]
    [SerializeField] private Text lostText;


    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        currentHPText.gameObject.SetActive(true);
        currentCoinsText.gameObject.SetActive(true);
        missionBriefText.gameObject.SetActive(true);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _isPause = !_isPause;
        }

        HideBrief();
        PauseGame();
        UpdateHealth();
        UpdateCoins();
    }


    private void HideBrief()
    {
        hideBrief -= Time.deltaTime;

        if (hideBrief <= 0)
        {
            missionBriefText.gameObject.SetActive(false);
            hideBrief = 0;
        }
    }
    private void PauseGame()
    {
        if (_isPause)
        {
            Time.timeScale = 0;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            currentHPText.gameObject.SetActive(false);
            currentCoinsText.gameObject.SetActive(false);

            pauseText.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            musicText.gameObject.SetActive(true);
            sfxText.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);

            if (hideBrief > 0)
            {
                missionBriefText.gameObject.SetActive(false);
            }
        }
        else
        {
            Time.timeScale = 1;

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            currentHPText.gameObject.SetActive(true);
            currentCoinsText.gameObject.SetActive(true);

            pauseText.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);
            musicText.gameObject.SetActive(false);
            sfxText.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
        }
    }

    private void UpdateHealth()
    {
        currentHPText.text = "CURRENT LIFES - " + Pawn_Stats.Instance.currentHealth.ToString();
    }
    private void UpdateCoins()
    {
        currentCoinsText.text = "COLLECTED COINS - " + Game_Manager.instance.currentCoins.ToString();
    }

    public void Win()
    {
        Time.timeScale = 0;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        winText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
        creatorText.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);
    }
    public void Lost()
    {
        Time.timeScale = 0;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        lostText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
        Sound_Settings.instance.PlayMusic("Theme");
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    #region Volumen settings related
    public void ToggleMusic()
    {
        Sound_Settings.instance.ToggleMusic();
    }
    public void ToggleSFX()
    {
        Sound_Settings.instance.ToggleSFX();
    }

    public void MusicVolume()
    {
        Sound_Settings.instance.MusicVolume(_musicSlider.value);
    }
    public void SFXVolume()
    {
        Sound_Settings.instance.SFXVolume(_sfxSlider.value);
    }
    #endregion
}