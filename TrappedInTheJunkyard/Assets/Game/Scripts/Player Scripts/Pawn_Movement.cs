using UnityEngine;

[RequireComponent (typeof(CharacterController))]
public sealed class Pawn_Movement : MonoBehaviour
{
    public static Pawn_Movement Instance;

    [Header("Input references")]
    public float horizontal;
    public float vertical;

    public float mouseX;
    public float mouseY;
    public float sensibility;

    [Header("Movement references")]
    [SerializeField] private CharacterController characterController;

    [SerializeField] private float speed;
    private float originalSpeed;
    private Vector3 _velocity;

    [SerializeField] private float gravityScale;

    [Header("Trap related references")]
    public bool slow;

    [SerializeField] private float slowDuration;
    private float resetSlowTime;

    [SerializeField] private float speedReduction;

    [Header("Camera references")]
    [SerializeField] private Transform _camera;
    [SerializeField] private float rotationX;
    [SerializeField] private float xMin;
    [SerializeField] private float xMax;

    private Vector3 _eulerAngles;


    private void Awake()
    {
        Instance = this;

        characterController = GetComponent<CharacterController>();

        characterController.detectCollisions = true;

        originalSpeed = speed;
        resetSlowTime = slowDuration;
    }

    private void Start()
    {
        _camera = GetComponentInChildren<Camera>().gameObject.transform;
    }

    private void Update()
    {
        #region Input script references

        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        mouseX = Input.GetAxis("Mouse X") * sensibility;
        mouseY = Input.GetAxis("Mouse Y") * sensibility;

        #endregion

        #region Movement script references

        Vector3 velocity = Vector3.ClampMagnitude(((transform.forward * vertical) + (transform.right * horizontal)) * speed, speed);

        _velocity.x = velocity.x;
        _velocity.z = velocity.z;

        if (characterController.isGrounded)
        {
            _velocity.y = 0f;
        }
        else
        {
            _velocity.y += Physics.gravity.y * gravityScale * Time.deltaTime;
        }

        characterController.Move(_velocity * Time.deltaTime);

        #endregion

        #region Camera script references

        rotationX += -Input.GetAxis("Mouse Y") * sensibility;
        rotationX = Mathf.Clamp(rotationX, xMin, xMax);
        _camera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * sensibility, 0);

        #endregion


        SlowMovement();
    }

    private void SlowMovement()
    {
        if (!slow)
        {
            slowDuration = resetSlowTime;
            speed = originalSpeed;
        }
        else
        {
            slowDuration -= Time.deltaTime;

            if (slowDuration > 0)
            {
                speed = speedReduction;
            }
            else
            {
                slow = false;
            }
        }
    }
}