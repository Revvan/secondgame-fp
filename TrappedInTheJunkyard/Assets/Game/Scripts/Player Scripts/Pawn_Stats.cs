using UnityEngine;

public sealed class Pawn_Stats : MonoBehaviour
{
    public static Pawn_Stats Instance;

    [Header("Pawn Stats")]
    //HP references
    public float currentHealth;
    [SerializeField] private float maxHealth;


    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(float value)
    {
        currentHealth -= value;
    }
}