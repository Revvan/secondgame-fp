using UnityEngine;

public sealed class Class_Coin : MonoBehaviour
{
    [SerializeField] private int value;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Game_Manager.instance.currentCoins += value;

            Sound_Settings.instance.PlaySFX("Coin SFX");

            Destroy(this.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("DecorationsBox") || other.gameObject.CompareTag("DocorationsMesh"))
        {
            Debug.Log("Coin has been destroyed");
            Destroy(this.gameObject);
        }
    }
}